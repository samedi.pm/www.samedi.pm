
# Front controller for the portal.
# Uses Flask and Jinja2.
# The database is in `data/`, as flat files.

# IMPORTS #####################################################################

import datetime
import logging
import random

from babel.dates import format_date
from markdown import markdown
from markdown.extensions.nl2br import Nl2BrExtension
from markdown.extensions.abbr import AbbrExtension
from os import environ, remove as removefile
from os.path import isfile, join, abspath, dirname
from flask import Flask, send_file, url_for, request, abort as abort_flask
from jinja2 import Environment, FileSystemLoader, Markup
from yaml import load as yaml_load


# PATH RELATIVITY #############################################################

THIS_DIRECTORY = dirname(abspath(__file__))


def get_path(relative_path):
    """Get an absolute path from a path relative to this script directory."""
    return abspath(join(THIS_DIRECTORY, relative_path))


# COLLECT ENVIRONMENT INFORMATION #############################################

is_debug = (environ.get('DEBUG') == 'true') or (environ.get('DEBUG') == '1')


# COLLECT GLOBAL INFORMATION FROM SOURCES #####################################

# VERSION

# with open(get_path('VERSION'), 'r') as version_file:
#     version = version_file.read().strip()

# CONFIG
with open(get_path('data/config.yml'), 'r') as yaml_file:
    config = yaml_load(yaml_file.read())

# CONTENT
with open(get_path('data/content.yml'), 'r') as yaml_file:
    content = yaml_load(yaml_file.read())


# I18N ########################################################################

# Nope, too much trouble. Let's use Babel.
# locale.setlocale(locale.LC_ALL, 'fr_FR')

my_locale = 'fr_FR'


# LOGGING #####################################################################

LOG_FILE = get_path('run.log')

log = logging.getLogger("YellowPortal")
if is_debug:
    log.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.ERROR)
logHandler = logging.FileHandler(LOG_FILE)
logHandler.setFormatter(logging.Formatter(
    "%(asctime)s - %(levelname)s - %(message)s"
))
log.addHandler(logHandler)


# SETUP FLASK ENGINE ##########################################################

app = Flask(__name__, root_path=THIS_DIRECTORY)
app.debug = is_debug
if app.debug:
    log.info("Starting Flask app IN DEBUG MODE...")
else:
    log.info("Starting Flask app...")


# def handle_error(e):
#     log.error(e)
#     return str(e)  # wish we could use the default error renderer here


# app.register_error_handler(Exception, handle_error)


# SETUP JINJA2 TEMPLATE ENGINE ################################################

def static_global(filename):
    return url_for('static', filename=filename)


def shuffle_filter(seq):
    """
    This shuffles the sequence it is applied to.
    """
    try:
        result = list(seq)
        random.shuffle(result)
        return result
    except:
        return seq


def markdown_filter(value, nl2br=False, p=True):
    """
    Converts markdown into html.
    nl2br: set to True to replace line breaks with <br> tags
    p: set to False to remove the enclosing <p></p> tags
    """
    extensions = [AbbrExtension()]
    if nl2br is True:
        extensions.append(Nl2BrExtension())
    markdowned = markdown(value, output_format='html5', extensions=extensions)
    if p is False:
        markdowned = markdowned.replace(r"<p>", "").replace(r"</p>", "")
    return markdowned


tpl_engine = Environment(loader=FileSystemLoader([get_path('view')]),
                         trim_blocks=True,
                         lstrip_blocks=True)

tpl_engine.globals.update(
    url_for=url_for,
    static=static_global,
)

tpl_engine.filters['markdown'] = markdown_filter
tpl_engine.filters['md'] = markdown_filter
tpl_engine.filters['shuffle'] = shuffle_filter

tpl_global_vars = {
    'request': request,
    # 'version': version,
    'config': config,
    'content': content,
    'now': datetime.datetime.now(),
}


# HELPERS #####################################################################

def abort(code, message):
    """
    A simple wrapper over Flask's abort().
    """
    log.error("Abort: " + message)
    abort_flask(code, message)


def render_view(view, context=None):
    """
    A simple helper to render [view] template with [context] vars.
    It automatically adds the global template vars defined above, too.
    It returns a string, usually the HTML contents to display.
    """
    context = {} if context is None else context
    params = tpl_global_vars.copy()
    params.update(context)
    return tpl_engine.get_template(view).render(params)


# ROUTES ######################################################################

@app.route("/hello")
def hello():
    return "Hello World from SAMEDI"


@app.route("/")
def main():
    now = datetime.datetime.now()
    t = datetime.timedelta((7 + 5 - now.weekday()) % 7)
    next_saturday = (now + t)
    return render_view('index.html.jinja2', {
        'next_saturday': format_date(next_saturday, 'dd MMMM yyyy', my_locale),
    })


# Everything not declared before (not a Flask route / API endpoint)...
# @app.route('/<path:path>')
# def route_frontend(path):
#     # ...could be a static file needed by the front end that
#     # doesn't use the `static` path (like in `<script src="bundle.js">`)
#     file_path = join(app.static_folder, path)
#     if isfile(file_path):
#         return send_file(file_path)
#     # ...or should be handled by the SPA's "router" in front end
#     else:
#         index_path = join(app.static_folder, 'index.html')
#         return send_file(index_path)


###############################################################################

if __name__ == "__main__":
    # Only for debugging during development
    extra_files = [get_path('data/config.yml'), get_path('data/content.yml')]
    app.run(host='0.0.0.0', debug=True, port=80, extra_files=extra_files)
