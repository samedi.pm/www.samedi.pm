# https://hub.docker.com/r/tiangolo/uwsgi-nginx-flask/
FROM tiangolo/uwsgi-nginx-flask:python3.7

#RUN pwd
# > "/app"

# We're doing this in the docker-compose
# Not sure which is best, nor what it entails.  Help.
#COPY ./app /app

#ENV STATIC_INDEX 1

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
RUN rm requirements.txt
